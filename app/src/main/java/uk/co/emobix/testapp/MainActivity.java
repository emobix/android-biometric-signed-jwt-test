package uk.co.emobix.testapp;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricPrompt;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.jose4j.jca.ProviderContext;
import org.jose4j.jwa.AlgorithmFactory;
import org.jose4j.jwa.AlgorithmFactoryFactory;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.EcdsaUsingShaAlgorithm;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jws.JsonWebSignatureAlgorithm;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.ECGenParameterSpec;
import java.util.concurrent.Executor;

import uk.co.emobix.testapp.ui.main.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        FloatingActionButton fab = findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createKey(view);
            }
        });
    }
    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private PrivateKey privateKey;
    private PublicKey publicKey;
    String keyId = "bio";

    private void verifyJws(String signedJwt) throws JoseException {
        JsonWebSignature jws = new JsonWebSignature();
        jws.setCompactSerialization(signedJwt);
        jws.setKey(publicKey);
        boolean signatureVerified = jws.verifySignature();

        System.out.println("JWS Signature is valid: " + signatureVerified);

        Toast.makeText(getApplicationContext(),
                "Signed! valid = "+signatureVerified, Toast.LENGTH_SHORT).show();

    }

    private void finishSign() throws JoseException, NoSuchAlgorithmException, InvalidKeyException {
        // Create the Claims, which will be the content of the JWT
        JwtClaims claims = new JwtClaims();
        claims.setIssuer("client-id");  // who creates the token and signs it
        claims.setAudience("our-server"); // to whom the token is intended to be sent
        claims.setExpirationTimeMinutesInTheFuture(10); // time when the token will expire (10 minutes from now)
        claims.setGeneratedJwtId(); // a unique identifier for the token
        claims.setIssuedAtToNow();  // when the token was issued/created (now)
        claims.setNotBeforeMinutesInThePast(2); // time before which the token is not yet valid (2 minutes ago)

        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setKey(privateKey);
        jws.setKeyIdHeaderValue(keyId);
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.ECDSA_USING_P256_CURVE_AND_SHA256);

        String signedJwt = jws.getCompactSerialization();

        Log.i("main", "jws = "+signedJwt);

        verifyJws(signedJwt);

    }

    private void startSign(PrivateKey privateKey) throws NoSuchAlgorithmException, InvalidKeyException {
        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setDeviceCredentialAllowed(false)
                .setTitle("Biometric auth")
                .setNegativeButtonText("Cancel")
                .build();

        executor = ContextCompat.getMainExecutor(this);

        biometricPrompt = new BiometricPrompt(MainActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Signature sig = result.getCryptoObject().getSignature();
                try {

                    // inject our biometric approved Signature object so it's used during the sign
                    AlgorithmFactoryFactory factoryFactory = AlgorithmFactoryFactory.getInstance();
                    AlgorithmFactory<JsonWebSignatureAlgorithm> jwsAlgorithmFactory = factoryFactory.getJwsAlgorithmFactory();
                    jwsAlgorithmFactory.registerAlgorithm(new EcdsaUsingShaAlgorithm.EcdsaP256UsingSha256() {
                        @Override
                        public byte[] sign(Key key, byte[] securedInputBytes, ProviderContext providerContext) throws JoseException
                        {
                            if (key == privateKey) {
                                Signature signature = sig;
                                try {
                                    signature.update(securedInputBytes);
                                    byte[] derEncodedSignatureBytes = signature.sign();
                                    final int signatureByteLength = 4;
                                    return convertDerToConcatenated(derEncodedSignatureBytes, signatureByteLength);
                                } catch (SignatureException | IOException e) {
                                    throw new JoseException("Problem creating signature.", e);
                                }
                            } else {
                                return super.sign(key, securedInputBytes, providerContext);
                            }
                        }

                    });


                    finishSign();

                } catch (JoseException | NoSuchAlgorithmException | InvalidKeyException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        Signature sig = Signature.getInstance("SHA256withECDSA");
        sig.initSign(privateKey);
        biometricPrompt.authenticate(promptInfo,
                new BiometricPrompt.CryptoObject(sig));
    }

    private void createKey(View view) {
        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_EC, "AndroidKeyStore");
            kpg.initialize(new KeyGenParameterSpec.Builder(
                    keyId,
                    KeyProperties.PURPOSE_SIGN | KeyProperties.PURPOSE_VERIFY)
                    .setDigests(KeyProperties.DIGEST_SHA256,
                            KeyProperties.DIGEST_SHA512)
                    .setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1"))
                    .setDigests(KeyProperties.DIGEST_SHA256)
                    .setUserAuthenticationRequired(true) // means BiometricPrompt must be used at signing time
//                    .setIsStrongBoxBacked(true) not usable on emulator
                    .setInvalidatedByBiometricEnrollment(true)
                    .build());

            KeyPair kp = kpg.generateKeyPair();

            privateKey = kp.getPrivate();
            publicKey = kp.getPublic();

            PublicJsonWebKey jwk = PublicJsonWebKey.Factory.newPublicJwk(publicKey);

            String jwkString = jwk.toJson();

            Log.i("main", "key = "+jwkString);

            //startSign(privateKey);
            finishSign();

        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException | JoseException | InvalidKeyException e) {
            Snackbar.make(view, "Exception", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            e.printStackTrace();
        }
    }
}