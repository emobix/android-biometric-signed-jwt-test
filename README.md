Simple project that uses jose4j to create a key stored in an Android
device's HSM that requires the user to present a biometric each time
it is used to sign a JWT. Uses the ES256 alg.

To run it:

1. Open project in android studio
1. Create/pick a destination to run on - emulator or device
1. Ensure your chosen device has finger print and a lock pattern or
pin enabled using the 'Security' section of the on-device settings app
1. Run the project (on a device or an emulator)
1. Press the 'email' green button to do the operation
1. Present fingerprint when prompted (on the emulator, hit the '...'
on the toolbar on the right hand side to open 'Extended Controls' then
select 'Fingerprint' and 'touch the sensor'.
1. If all is good, a toast saying 'Signed! valid = true' will pop up.
1. The public JWK and signed JWT will be in the log (the 'Run' tab
in Android Studio if running, or the 'console' tab within the 'debug'
tab if debugging)
